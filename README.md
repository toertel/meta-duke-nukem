# meta-duke-nukem

This meta-layer supplies recipes for compiling Duke Nukem 3D from 3D Realms with Yocto Project.

The meta-layer is has been tested on Poky using qemux86_64 with Yocto Project 3.1 "Dunfell" and 3.2 "Gatesgarth".

## Setup

Create a directory `yocto` (or any other name of your choice) and clone Yocto Project Poky and meta-duke-nukem into it.

```
mkdir yocto
cd yocto
git clone https://git.yoctoproject.org/git/poky
git clone git://git.openembedded.org/meta-openembedded
git clone https://gitlab.com/toertel/meta-duke-nukem.git
```

After cloning the meta-layers make sure to switch to the branches matching the version of Yocto Project you would like to use. Here, *gatesgarth* is used.

```
cd poky
git checkout gatesgarth
cd ../meta-openembedded
git checkout gatesgarth
cd ../meta-duke-nukem
git checkout gatesgarth
cd ..
```

Copy supplied `bblayers.conf` or create your own.

```
cp meta-duke-nukem/conf/bblayers.conf.sample poky/build/conf/bblayers.conf
```

Copy Poky's `local.conf.sample` or create your own.

```
cp poky/meta-poky/conf/local.conf.sample poky/build/conf/local.conf
```

## Build

Source build environment and build _core-image-eduke32_.

```
cd poky
source oe-init-build-env
bitbake core-image-eduke32
```

## Run

### X11

Start QEMU with the image built.

```
runqemu core-image-eduke32 kvm slirp serial gl sdl audio
```

Login as user _root_ and and start `eduke32` inside `/usr/share/games/duke-nukem-3d`.

```
cd /usr/share/games/duke-nukem-3d
DISPLAY=:0 eduke32
```

**Note:** In starting of QEMU fails because _package dri was not found_ you need to instal the MESA development package of your distribution. For Ubuntu `sudo apt install mesa-common-dev` does the trick.
