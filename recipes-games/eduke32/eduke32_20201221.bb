SUMMARY = "Duke Nukem 3D port with enhanced graphics"
DESCRIPTION = "EDuke32 is an awesome, free homebrew game engine and source \
port of the classic PC first person shooter Duke Nukem 3D."
HOMEPAGE = "https://www.eduke32.com/"
BUGTRACKER = "https://voidpoint.io/terminx/eduke32/-/issues"

SECTION = "games"

LICENSE = "GPL-2.0+ & BUILDLIC"
LIC_FILES_CHKSUM = " \
    file://source/duke3d/gpl-2.0.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263 \
    file://source/kenbuild/buildlic.txt;md5=68b498942a59b13f60086049f5711ab7 \
"

NO_GENERIC_LICENSE[BUILDLIC] = "source/kenbuild/buildlic.txt"

DEPENDS = "virtual/libsdl2 libvpx flac virtual/libgl libglu"

SRC_URI = " \
    git://voidpoint.io/terminx/eduke32.git;protocol=https \
    file://use-pkg-config.patch \
"
SRCREV = "2bb6cbcae798e4dca542490aa5faffe2451466ea"
S = "${WORKDIR}/git"

inherit pkgconfig

EXTRA_OEMAKE = " \
    CC='${CC}' \
    CXX='${CXX}' \
    AR='${AR}' \
    RANLIB='${RANLIB}' \
    STRIP='' \
"

EXTRA_OEMAKE += "${@oe.utils.conditional("DEBUG_BUILD", "1", "RELEASE=0 OPTLEVEL=0", "", d)}"

do_install() {
    install -d ${D}/${bindir}
    install -m 0755 eduke32 ${D}/${bindir}
    install -m 0755 mapster32 ${D}/${bindir}
}

PACKAGES_prepend = "${PN}-mapster32 "

FILES_${PN}-mapster32 += "${bindir}/mapster32"

RRECOMMENDS_${PN} = "duke-nukem-3d-episode-1"
