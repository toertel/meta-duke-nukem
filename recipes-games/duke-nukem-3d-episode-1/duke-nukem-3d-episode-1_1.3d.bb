SUMMARY = "Duke Nukem 3D level data for Episode 1 'L.A. Meltdown'."
DESCRIPTION = "Level data for the Duke Nukem 3D shareware version for the \
first episode of the game, L.A. Meltdown. There are five regular levels and \
one secret level."

SECTION = "games"

LICENSE = "Duke-Nukem-3D-Shareware-License"
LIC_FILES_CHKSUM = "file://LICENSE.TXT;md5=583bf2a6cb3d404a21c2205041c45481"

NO_GENERIC_LICENSE[Duke-Nukem-3D-Shareware-License] = "LICENSE.TXT"

DEPENDS = "zip-native"

SRC_URI = "https://archive.org/download/3dduke13/3dduke13.zip"
SRC_URI[md5sum] = "04e4ca70b8a2d59ed56c451c5c1d5d39"
SRC_URI[sha256sum] = "c67efd179022bc6d9bde54f404c707cbcbdc15423c20be72e277bc2bdddf3d0e"

inherit allarch

do_patch() {
    unzip DN3DSW13.SHR -d ${B}
}

do_install() {
    install -d ${D}/${datadir}/games/duke-nukem-3d
    install -m 0644 DUKE3D.GRP ${D}/${datadir}/games/duke-nukem-3d/duke3d.grp
    install -m 0644 DUKE.RTS ${D}/${datadir}/games/duke-nukem-3d/duke.rts
}

FILES_${PN} = "${datadir}/games/duke-nukem-3d"
