SUMMARY = "Image which documents how to build ezQuake"

LICENSE = "MIT"

inherit core-image

IMAGE_FEATURES += "x11-base"

IMAGE_INSTALL_append = " eduke32"
